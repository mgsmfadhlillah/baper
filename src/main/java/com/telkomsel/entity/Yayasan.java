package com.telkomsel.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "yayasan")
public class Yayasan {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "yayasan_id", updatable = false, nullable = false)
  private Integer yayasanId;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  private User userId;

  @Column(name = "ktp_number", length = 20, nullable = false)
  private String ktp;

  @Column(name = "account_number", length = 20, nullable = false)
  private String accountNumber;

  @Column(name = "certificate_number", length = 50, nullable = false)
  private String certificateNumber;

  @Column(name = "npwp_number", length = 20, nullable = false)
  private String npwpNumber;

  @Column(name = "created_date")
  @Temporal(TemporalType.TIMESTAMP)
  private Date createdTime;

  @Column(name = "created_by", length = 50)
  private String createdBy;
}
