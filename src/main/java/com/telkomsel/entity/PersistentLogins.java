package com.telkomsel.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "persistent_logins")
public class PersistentLogins {

  @Id
  @Column(name = "series", length = 64, nullable = false)
  private String series;

  @Column(name = "username", length = 64, nullable = false)
  private String username;

  @Column(name = "token", length = 64, nullable = false)
  private String token;

  @Column(name = "last_used")
  @Temporal(TemporalType.TIMESTAMP)
  private Date lastUsed;

}
