package com.telkomsel.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "photos")
public class Photos {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "photo_id", updatable = false, nullable = false)
  private Integer photo_id;

  @Column(name = "nama_photo", length = 80)
  private String nama;

  @Column(name = "judul", length = 30)
  private String judul;

  @Column(name = "deskripsi", length = 100)
  private String deskripsi;

  @Column(name = "created_date")
  @Temporal(TemporalType.TIMESTAMP)
  private Date createdTime;

  @Column(name = "created_by", length = 50)
  private String createdBy;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "kegiatan_id")
  private BaperActivity baperActivity;
}
