package com.telkomsel.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "content")
public class Content {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "content_id", updatable = false, nullable = false)
  private Integer contentId;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "admin_id")
  private Admin adminId;

  @Column(name = "title", length = 100, nullable = false)
  private String title;

  @Column(name = "content", nullable = false)
  private String content;

  @Column(name = "created_date")
  @Temporal(TemporalType.TIMESTAMP)
  private Date createdTime;

  @Column(name = "created_by", length = 50)
  private String createdBy;

  @Column(name = "updated_date")
  @Temporal(TemporalType.TIMESTAMP)
  private Date updatedTime;

  @Column(name = "updated_by", length = 50)
  private String updatedBy;

}
