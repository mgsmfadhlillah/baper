package com.telkomsel.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "donatur")
public class Donatur {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "donatur_id", updatable = false, nullable = false)
  private Integer donaturId;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  private User userId;

  @Column(name = "point", length = 11, nullable = false)
  private Integer point;

//  @Column(name = "nama", length = 150, nullable = false)
//  private String nama;
//
//  @Column(name = "rekening_pengirim", length = 16, nullable = false)
//  private String rekPengirim;
//
//  @Column(name = "rekening_penerima", length = 16, nullable = false)
//  private String rekPenerima;
//
//  @Column(name = "trx", length = 10, nullable = false)
//  private String trx;
//
//  @Column(name = "tanggal")
//  @Temporal(TemporalType.DATE)
//  private Date tanggal;
//
//  @Column(name = "keterangan", length = 150, nullable = false)
//  private String keterangan;
//
//  @Column(name = "created_date")
//  @Temporal(TemporalType.TIMESTAMP)
//  private Date createdTime;
//
//  @Column(name = "created_by", length = 50)
//  private String createdBy;
//
//  @OneToOne(fetch = FetchType.LAZY)
//  @JoinColumn(name = "cabang_id")
//  private Cabang cabang;
}
