package com.telkomsel.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "user", uniqueConstraints = { @UniqueConstraint(name = "user_uk", columnNames = {"email","phone_number"}) })
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "user_id", updatable = false, nullable = false)
  private Integer userId;

  @Column(name = "fullname", length = 255, nullable = false)
  private String fullname;

  @Column(name = "email", length = 255, nullable = false)
  private String email;

  @Column(name = "password", length = 250, nullable = false)
  private String password;

  @Column(name = "birth_date")
  private Date birthDate;

  @Column(name = "address", length = 250, nullable = false)
  private String address;

  @Column(name = "enabled", length = 1, nullable = false)
  private int enabled;

  @Column(name = "phone_number", length = 16)
  private String phoneNumber;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "role_id")
  private Role roleId;

  @Column(name = "join_date")
  @Temporal(TemporalType.TIMESTAMP)
  private Date createdTime;

  @Column(name = "created_by", length = 50)
  private String createdBy;
}