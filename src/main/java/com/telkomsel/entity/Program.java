package com.telkomsel.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "program")
public class Program {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "program_id", updatable = false, nullable = false)
  private Integer program_id;

  @Column(name = "nama")
  private String nama;

  @Column(name = "deskripsi")
  private String deskripsi;

  @Column(name = "created_date")
  @Temporal(TemporalType.TIMESTAMP)
  private Date createdTime;

  @Column(name = "created_by", length = 50)
  private String createdBy;
}
