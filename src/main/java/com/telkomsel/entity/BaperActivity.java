package com.telkomsel.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "baper_activity")
public class BaperActivity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "baper_activity_id", updatable = false, nullable = false)
  private Integer baperActivityId;

  @Column(name = "activity_name", length = 255)
  private String activityName;

  @Column(name = "activity_date")
  @Temporal(TemporalType.TIMESTAMP)
  private Date activityDate;

  @Digits(integer=15, fraction=2)
  @Column(name = "activity_allocation")
  private BigDecimal weight;

  @Column(name = "created_date")
  @Temporal(TemporalType.TIMESTAMP)
  private Date createdTime;

  @Column(name = "created_by", length = 50)
  private String createdBy;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "cabang_id")
  private Content content;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "panti_id")
  private Yayasan yayasan;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "program_id")
  private Program program;
}
