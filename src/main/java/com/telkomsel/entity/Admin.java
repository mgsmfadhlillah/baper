package com.telkomsel.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "admin")
public class Admin {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "admin_id", updatable = false, nullable = false)
  private Integer adminId;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  private User userId;

  @Column(name = "branch", length = 100, nullable = false)
  private String branch;

  @Column(name = "title", length = 100, nullable = false)
  private String title;

}
