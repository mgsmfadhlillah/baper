package com.telkomsel.controller;

import com.telkomsel.BaperApplication;
import com.telkomsel.repo.AppUserRepository;
import com.telkomsel.utils.AuthUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.context.request.WebRequest;

import java.security.Principal;

@Controller
public class MainController {
  private static Logger log = LoggerFactory.getLogger(BaperApplication.class);
  @Autowired private AppUserRepository userRepo;

  @GetMapping(path = "/backend")
  public String pageLogin(WebRequest wr) {
    log.info("INFO PAGE :: "+wr.toString());
    return "login";
  }

  @GetMapping(path = "/")
  public String frontPage(WebRequest wr){
    log.info("INFO PAGE :: "+wr.toString());
    return "frontpage";
  }

  @GetMapping(path = {"/index"})
  public String pageIndex(Principal principal, WebRequest wr) {
    User auth = (User) ((Authentication) principal).getPrincipal();
    String role = AuthUtils.getRole(auth);
    String direction = "";

    switch (role){
      case "ROLE_ADMIN" : direction = "/dashboard"; break;
      default: direction = "/logout"; break;
    }

    log.debug("INFO PAGE :: "+wr.toString());
    return "redirect:"+direction;
  }

  @GetMapping(path = "/dashboard")
  public String pageDashboard(Principal principal, WebRequest wr, Model model) {
    User auth = (User) ((Authentication) principal).getPrincipal();
    String role = AuthUtils.getRole(auth);
    log.info("INFO PAGE :: "+wr.toString());
    model.addAttribute("page", "dashboard");
    return "index";
  }

  @GetMapping(path = "/user")
  public String pageUser(Principal principal, WebRequest wr, Model model) {
    User auth = (User) ((Authentication) principal).getPrincipal();
    String role = AuthUtils.getRole(auth);
    log.info("INFO PAGE :: "+wr.toString());
    model.addAttribute("page", "user");
    return "index";
  }
}
