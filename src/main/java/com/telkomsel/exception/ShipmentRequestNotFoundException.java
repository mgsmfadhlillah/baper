package com.telkomsel.exception;

import com.telkomsel.BaperApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ShipmentRequestNotFoundException extends RuntimeException {
  private static Logger log = LoggerFactory.getLogger(BaperApplication.class);
  public ShipmentRequestNotFoundException(String url, String msg){
    super(msg);
    log.debug(url+" :: FAILED :: "+msg);
  }
}
