package com.telkomsel.services;

import com.telkomsel.entity.User;
import com.telkomsel.repo.AppRoleRepository;
import com.telkomsel.repo.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
 
    @Autowired
    private AppUserRepository appUserRepo;
 
    @Autowired
    private AppRoleRepository appRoleRepo;
 
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = appUserRepo.findByEmailAndEnabled(email, 1);
        if (user == null) {
            throw new UsernameNotFoundException("User " + email + " was not found in the database or disabled");
        }
        List<String> roleNames = appRoleRepo.getRoleNames(user.getUserId());
 
        List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
        if (roleNames != null) {
            for (String role : roleNames) {
                GrantedAuthority authority = new SimpleGrantedAuthority(role);
                grantList.add(authority);
            }
        }
        UserDetails userDetails = (UserDetails) new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), grantList);
        return userDetails;
    }
 
}