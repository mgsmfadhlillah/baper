package com.telkomsel.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class Bcrypter {

  public static String encrytePassword(String password) {
    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    return encoder.encode(password);
  }

  public static Boolean matchPassword(String p1, String p2) {
    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    return encoder.matches(p1,p2);
  }

}