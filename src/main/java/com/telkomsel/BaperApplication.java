package com.telkomsel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaperApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaperApplication.class, args);
	}

}
