package com.telkomsel.repo;

import com.telkomsel.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppUserRepository extends JpaRepository<User, Integer> {
  User findByEmail(String email);
  User findByEmailAndEnabled(String email, Integer sts);
}
