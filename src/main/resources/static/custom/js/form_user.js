$(document).ready(function() {
  $.get(getWarehouse+type, function(){
    console.log("success");
  }).done(function(list){
    var data = [];
    $.each(list, function (k, v) {
      data.push({id: list[k].warehouseId, text: list[k].nama+' - '+list[k].courier})
    });
    $("#warehouse.select2-container").select2({"theme": "bootstrap", data: data});
    $("#warehouse.select2-container").select2({width: '100%'});
  }).fail(function(ex){
    console.log(ex);
  });

  $("#submit-form").on('click', function(){
    var body = {
      "email" : $("#email").val(),
      "encryptedPassword" : $("#password").val(),
      "warehouse" : {
        "warehouseId" : $("#warehouse").val()
      }
    };
    if($("#submit-form").data("crud") == "new"){
      var url = sendUser;
    }else{
      var url = updateUser;
      $.extend( body, {userId : $("#id").val()});
    }
    fetch(url,{method:"POST", headers:{'Content-Type':'application/json'}, body:JSON.stringify(body)})
        .then(resp => {
          if(resp.status == 200){
            Swal.fire({
              title: 'Good Job!',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Ok'
            }).then((result) => {
              if (result.value) {
                location.reload();
              }
            });
            return resp.json();
          }else{
            Swal.fire('Failed!', 'Server Error : '+resp.statusText, 'failed');
            return Promise.reject("server");
          }
        })
        .then(data => {
          dataRecieved = JSON.parse(data);
          alert("RECIEVED :"+dataRecieved);
        }).catch(err => {
      if (err == "server") Swal.fire('Failed!', 'Server Error', 'failed');
    });
  });
});