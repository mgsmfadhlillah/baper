var table;
$(document).ready(function() {
  $(function() {
    table = $('#myTable').removeAttr("width").DataTable({
      "sAjaxSource": getData+"/"+type,
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      "pageLength": 25,
      "lengthChange": true,
      "deferRender": true,
      "responsive": true,
      "searching": true,
      "processing": true,
      "sAjaxDataProp": "",
      "aoColumns": [
        { "mData": "email" },
        { "mData": "warehouse.nama" },
        { "mData": "warehouse.picName" },
        { "mData": "warehouse.picPhone" },
        { "mData": "warehouse.bu" }
      ],
      "drawCallback": function () {
        $('.dataTables_paginate > .pagination').addClass('pagination-sm');
      },
      select: 'single',
      "dom": "<'top'<'row'<'col-sm-6 d-flex justify-content-start'l><'col-sm-6'f>>>rt<'bottom'<'row'<'col-sm-6'i><'col-sm-6'p>>><'clear'>"
    });
  });

  $("#delete-user").on('click', function(){
    let row = table.rows('.selected').data()[0];
    if(row === undefined){
      Swal.fire('Oops...', 'Please select your data.', 'error');
    }else{
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          $.get(deleteUser+row.userId, function (){ console.log('success delete'); })
              .done(function(){
                Swal.fire('Deleted!', 'Your data has been deleted.', 'success');
                location.reload();
              }).fail(function(ex) {
            console.log('error '+ex);
          });
        }
      });
    }
  });

  $('#modal-user').on('hide.bs.modal', function() {
    location.reload();
  });

  function editMdUser(data){
    $("#email").val(data.email);
    $("#warehouse.select2-container").select2().val(data.warehouse.warehouseId).trigger('change');
    $("#password").val("");
  }

  $("#edit-user").on('click', function(){
    $("#submit-form").attr('data-crud','edit');
    let row = table.rows('.selected').data()[0];
    if(row === undefined){
      Swal.fire('Oops...', 'Please select your data.', 'error');
    }else{
      $("#id").val(row.userId);
      editMdUser(row);
      $('#modal-user').modal('show');
    }
  });
});