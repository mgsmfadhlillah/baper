var table;
$(document).ready(function() {
  $(function() {
    table = $('#myTable').removeAttr("width").DataTable({
      "sAjaxSource": getData,
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      "pageLength": 25,
      "lengthChange": true,
      "deferRender": true,
      "responsive": true,
      "searching": true,
      "processing": true,
      "sAjaxDataProp": "",
      "aoColumns": [
        { "mData": "courier" },
        { "mData": "kodePos" },
        { "mData": "warehouseId" },
        { "mData": "kota" },
        { "mData": "kecamatan" }
      ],
      "drawCallback": function () {
        $('.dataTables_paginate > .pagination').addClass('pagination-sm');
      },
      "dom": "<'top'<'row'<'col-sm-6 d-flex justify-content-start'l><'col-sm-6'f>>>rt<'bottom'<'row'<'col-sm-6'i><'col-sm-6'p>>><'clear'>"
    });
  });
});