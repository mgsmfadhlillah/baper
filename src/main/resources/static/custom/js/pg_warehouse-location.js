var table;
$(document).ready(function() {
  $(function() {
    table = $('#myTable').removeAttr("width").DataTable({
      "scrollX": true,
      "sAjaxSource": getData+"/"+type,
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      "pageLength": 25,
      "lengthChange": true,
      "deferRender": true,
      "responsive": true,
      "searching": true,
      "processing": true,
      "sAjaxDataProp": "",
      "aoColumns": [
        { "mData": "courier" },
        { "mData": "nama" },
        { "mData": "alamat" },
        { "mData": "kodePos" },
        { "mData": "latlong" },
        { "mData": "picName" },
        { "mData": "picPhone" },
        { "mData": "operationDays" },
        { "mData": "operationHourStart" },
        { "mData": "operationHourEnd" },
        { "mData": "isactive" },
        { "mData": "bu" }
      ],
      columnDefs: [
        { width: "100px", targets: 0 },
        { width: "150px", targets: 1 },
        { width: "350px", targets: 2 },
        { width: "100px", targets: 5 },
        { width: "50px", targets: 11 }
      ],
      fixedColumns: true,
      select: 'single',
      "drawCallback": function () {
        $('.dataTables_paginate > .pagination').addClass('pagination-sm');
      },
      "dom": "<'top'<'row'<'col-sm-6 d-flex justify-content-start'l><'col-sm-6'f>>>rt<'bottom'<'row'<'col-sm-6'i><'col-sm-6'p>>><'clear'>"
    });
  });
  $('#modal-warehouse-location').on('hide.bs.modal', function() {
    location.reload();
  });
  function editMdWarehouse(data){
    $("#picName").val(data.picName);
    $("#picPhone").val(data.picPhone);
    $("#courierName").val(data.courier);
    $("#buID.select2-container").select2().val(data.bu).trigger('change');
    $("#senderName").val(data.nama);
    $("#kota").val(data.kota);
    $("#kecamatan").val(data.kecamatan);
    $("#kodePos").val(data.kodePos);
    $("#address").val(data.alamat);
    $("#opDays").val(data.operationDays);
    $("#opHourStart").val(data.operationHourStart);
    $("#opHourEnd").val(data.operationHourEnd);
    $("#status.select2-container").select2().val(data.isactive).trigger('change');
    $("#type").val(data.warehouseType);
    if(type == 'ojol') $("#latlong").val(data.latlong);
  }
  $('#edit-warehouse').on('click', function() {
    $("#submit-form").attr('data-crud','edit');
    let row = table.rows('.selected').data()[0];
    if(row === undefined){
      Swal.fire('Oops...', 'Please select your data.', 'error');
    }else{
      $("#id").val(row.warehouseId);
      editMdWarehouse(row);
      $('#modal-warehouse-location').modal('show');
    }
  });

  $("#delete-warehouse").on('click', function(){
    let row = table.rows('.selected').data()[0];
    if(row === undefined){
      Swal.fire('Oops...', 'Please select your data.', 'error');
    }else{
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          $.get(deleteWarehouse+row.warehouseId, function (){ console.log('success delete'); })
              .done(function(){
                Swal.fire('Deleted!', 'Your data has been deleted.', 'success');
                location.reload();
              }).fail(function(ex) {
            console.log('error '+ex);
          });
        }
      });
    }
  });
});