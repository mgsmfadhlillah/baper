$(document).ready(function() {
  $.get(getCourierName, function (){ console.log("success"); }).done(function(data){
    $("#courierName.select2-container").select2({"theme": "bootstrap", data: data});
    $("#courierName.select2-container").select2({width: '100%'});
  }).fail(function(ex){
    console.log(ex);
  });

  function getKeyByType(val){
    $.get(getTypeRef, { type : val }).done(function(data){
      $("#inputKey.select2-container").select2({"theme": "bootstrap", data: data});
      $("#inputKey.select2-container").select2({width: '100%'});
    }).fail(function(ex){
      console.log(ex);
    });
  }
  getKeyByType($("#inputType").val());
  $("#inputType").change(function(){
    $("#inputKey").html("");
    $("#inputKey").val("");
    var val = $(this).val().trim();
    val = val.replace(/\s+/g, '');
    getKeyByType(val);
  });

  function getServiceByName(val){
    $.get(getCourierService, { type : val }).done(function(data){
      $("#courierService.select2-container").select2({"theme": "bootstrap", data: data});
      $("#courierService.select2-container").select2({width: '100%'});
    }).fail(function(ex){
      console.log(ex);
    });
  }

  getServiceByName("GOSEND");
  $("#courierName").change(function(){
    $("#courierService").html("");
    $("#courierService").val("");
    var val = $(this).val().trim();
    val = val.replace(/\s+/g, '');
    getServiceByName(val);
  });

  $("#submit-form").on('click', function(){
    var body =  { "type": $("#inputType").val(), "key": $("#inputKey").val(), "value": $("#inputValue").val(),
                  "startdate": $("#startDateId").val(), "enddate": $("#endDateId").val(), "name": $("#courierName").val(),
                  "service": $("#courierService").val()
                };
    if($("#submit-form").data("crud") == "new"){
      var url = sendPaymentCondition;
    }else{
      var url = updatePaymentCondition;
      $.extend( body, {id : $("#id").val()});
    }
    fetch(url,{method:"POST", headers:{'Content-Type':'application/json'}, body:JSON.stringify(body)})
        .then(resp => {
          if(resp.status == 200){
            Swal.fire({
              title: 'Good Job!',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Ok'
            }).then((result) => { if(result.value) location.reload(); });
            return resp.json();
          }else{
            Swal.fire('Failed!', 'Server Error : '+resp.statusText, 'failed');
            return Promise.reject("server");
          }
        })
        .then(data => {
          dataRecieved = JSON.parse(data);
          alert("RECIEVED :"+dataRecieved);
        }).catch(err => {
          if (err == "server") Swal.fire('Failed!', 'Server Error', 'failed');
        });
  });
});