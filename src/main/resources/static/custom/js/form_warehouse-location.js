$(document).ready(function(){

  $("#submit-form").on('click', function(){

    var serializer = $("form#form-warehouse-location").serializeArray();
    var body =  {
      "picName": serializer[0].value,
      "picPhone": serializer[1].value,
      "courier": serializer[2].value,
      "bu": serializer[3].value,
      "nama": serializer[4].value,
      "kota": serializer[5].value,
      "kecamatan": serializer[6].value,
      "kodePos": serializer[7].value,
      "alamat": serializer[8].value,
      "operationDays": serializer[9].value,
      "operationHourStart": serializer[10].value,
      "operationHourEnd": serializer[11].value,
      "isactive": serializer[12].value,
      "warehouseType": serializer[13].value
    };
    if(type == 'ojol') $.extend( body, {latlong : serializer[14].value});
    if($("#submit-form").data("crud") == "new"){
      var url = sendWarehouseLocation;
    }else{
      var url = updateWarehouseLocation;
      $.extend( body, {warehouseId : $("#id").val()});
    }
    fetch(url,{method:"POST", headers:{'Content-Type':'application/json'}, body:JSON.stringify(body)})
        .then(resp => {
          if(resp.status == 200){
            Swal.fire({
              title: 'Good Job!',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Ok'
            }).then((result) => {
              if (result.value) {
                location.reload();
              }
            });
            return resp.json();
          }else{
            Swal.fire('Failed!', 'Server Error : '+resp.statusText, 'failed');
            return Promise.reject("server");
          }
        })
        .then(data => {
          dataRecieved = JSON.parse(data);
          alert("RECIEVED :"+dataRecieved);
        }).catch(err => {
      if (err == "server") Swal.fire('Failed!', 'Server Error', 'failed');
    });
  });
});