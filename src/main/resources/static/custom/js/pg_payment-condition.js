var table;
function dateConverter(datex) {
  return moment(datex).format('D MMM, YYYY');
}
$(document).ready(function() {
  $.fn.dataTable.moment( 'D MMM, YYYY' );
  $(function() {
    table = $('#myTable').DataTable({
      "sAjaxSource": getData,
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      "pageLength": 25,
      "lengthChange": true,
      "deferRender": true,
      "responsive": true,
      "searching": true,
      "processing": true,
      "sAjaxDataProp": "",
      "aoColumns": [
        { "mData": "type" },
        { "mData": "key" },
        { "mData": "value" },
        {
          "mData" : "startdate",
          "type"  : "date",
          "render": function(data, type, row){
            return dateConverter(data);
          }
        },
        {
          "mData" : "enddate",
          "type"  : "date",
          "render": function(data, type, row){
            return dateConverter(data);
          }
        },
        { "mData": "name" },
        { "mData": "service" }
      ],
      select: 'single',
      "drawCallback": function () {
        $('.dataTables_paginate > .pagination').addClass('pagination-sm');
      },
      "dom": "<'top'<'row'<'col-sm-6 d-flex justify-content-start'l><'col-sm-6'f>>>rt<'bottom'<'row'<'col-sm-6'i><'col-sm-6'p>>><'clear'>"
    });
  });

  $("#delete-payment").on('click', function(){
    let row = table.rows('.selected').data()[0];
    if(row === undefined){
      Swal.fire('Oops...', 'Please select your data.', 'error');
    }else{
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          $.get(deletePayment+row.id, function (){ console.log('success delete'); })
              .done(function(){
                Swal.fire('Deleted!', 'Your data has been deleted.', 'success');
                location.reload();
              }).fail(function(ex) {
            console.log('error '+ex);
          });
        }
      });
    }
  });

  $('#modal-payment-condition').on('hide.bs.modal', function() {
    location.reload();
  });

  function editMdPayment(data){
    $("#courierName.select2-container").select2().val(data.name).trigger('change');
    $("#courierService.select2-container").select2().val(data.service).trigger('change');
    $("#inputType.select2-container").select2().val(data.type).trigger('change');
    $("#inputKey.select2-container").select2().val(data.key).trigger('change');
    $("#inputValue.select2-container").select2().val(data.value).trigger('change');
    $('#reportrange span').html(moment(data.startdate).format('MMM D, YYYY') + ' - ' + moment(data.enddate).format('MMM D, YYYY'));
    $("#startDateId").val(data.startdate);
    $("#endDateId").val(data.enddate);
  }

  $("#edit-payment").on('click', function(){
    $("#submit-form").attr('data-crud','edit');
    let row = table.rows('.selected').data()[0];
    if(row === undefined){
      Swal.fire('Oops...', 'Please select your data.', 'error');
    }else{
      $("#id").val(row.id);
      editMdPayment(row);
      $('#modal-payment-condition').modal('show');
    }
  });
});